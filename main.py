import discord
import os
import subprocess
import datetime
import json
from enum import Enum

from config import Context, Option, on_config_message, get_config, supported_channels_tyes
from mail import send_messages_to_mail, send_discord_mail, msg_to_text
from constants_emoji import emoji_begin, emoji_sent, emoji_watch, used_emoji

import secret


intents = discord.Intents.default()
intents.members = True
intents.messages = True
client = discord.Client(intents = intents)


@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))


begins = {}

def is_direct_mention(message, user):
    return user in message.mentions and f"<@!{user.id}>" in message.content

def is_mention(message, user):
    return (
            is_direct_mention(message, user) or
            user in [x for r in message.role_mentions for x in r.members] or
            message.mention_everyone or
            False )

async def is_reply(message, user, channel):
    if not message.reference:
        return False
    try:
        ref = await channel.fetch_message(message.reference.message_id)
        return ref.author == user or is_reply(ref, user, channel)
    except:
        return False

async def is_watch(message, user, channel, context):
    # print(message.reactions)
    for r in message.reactions:
        if r.emoji == emoji_watch:
            async for ru in r.users():
                if ru == user:
                    return True
    if context in [Context.auto, Context.edit, Context.delete]:
        if message.reference:
            try:
                ref = await channel.fetch_message(message.reference.message_id)
                if await is_watch(ref, user, channel, context):
                    return True
            except:
                pass
    return False

async def if_receive(channel, user, default:str, context:Context, message = None):
    autoreceive = get_config(str(user.id), channel, Option.receive, default, context)
    r = (
            "a" in autoreceive  or
            message != None and "D" in autoreceive and user in message.mentions or
            message != None and "d" in autoreceive and is_direct_mention(message, user) or
            message != None and "M" in autoreceive and (user in message.mentions or is_mention(message, user)) or
            message != None and "m" in autoreceive and is_mention(message, user) or
            message != None and "r" in autoreceive and await is_reply(message, user, channel) or
            message != None and "w" in autoreceive and await is_watch(message, user, channel, context) or
            False )
    # print(autoreceive, r)
    return r

async def on_raw_reaction_add_control(payload):
    user_id = str(payload.user_id)
    begins_ind = payload.channel_id,payload.user_id
    # print(str(payload.emoji), "USER:" , payload.user_id)
    if str(payload.emoji) == emoji_begin:
        begins[begins_ind]=(payload.message_id,datetime.datetime.now())
    if str(payload.emoji) == emoji_sent:
        context = Context.manual
        # print(payload.message_id)
        # print(payload.user_id)
#neomutt -e "set from = \"$(cat name)  <jirikalvoda+sms@kam.mff.cuni.cz>\"" -s SMS  jirikalvoda+sms@kam.mff.cuni.cz
        #with subprocess.Popen(["neomutt","jirikalvoda@kam.mff.cuni.cz"], stdin=subprocess.PIPE) as p:
        # print(m.author.nick)
        channel = await client.fetch_channel(payload.channel_id)
        hist = [await channel.fetch_message(payload.message_id)] + (await channel.history(before=discord.Object(payload.message_id),limit=25).flatten())
        if ( begins_ind in begins
                and datetime.datetime.now()-begins[begins_ind][1] < datetime.timedelta(0,0,0,  0, 1)
                and begins[begins_ind][0] in [ x.id for x in hist ] ):
            hist = hist[0:[ x.id for x in hist ].index(begins[begins_ind][0])+1]
        else:
            hist = hist[0:1]
        if True: # if_receive
            await send_messages_to_mail(hist,user_id,context=Context.manual) 

async def on_reaction(payload, context):
    channel = await client.fetch_channel(payload.channel_id)
    if type(channel) in supported_channels_tyes:
        if payload.emoji.name not in used_emoji:
            message = await channel.fetch_message(payload.message_id)
            from_user = await client.fetch_user(payload.user_id)
            for user in channel.members:
                user_id = str(user.id)
                if await if_receive(channel, user, "no", context, message):
                    from_name = from_user.name
                    text = f"{from_name} {'add' if context == Context.emoji_add else 'remove'} emoji {payload.emoji.name} to:\n\n"
                    text += "\n".join([ '> '+ i for i in (await msg_to_text(message, user_id, context)).split("\n")])+"\n"
                    await send_discord_mail(user_id, text, channel=channel, m_id=payload.message_id, in_reply_to_id=payload.message_id, context=context, from_name=from_name)

@client.event
async def on_raw_reaction_add(payload):
    await on_raw_reaction_add_control(payload)
    await on_reaction(payload, Context.emoji_add)
        
@client.event
async def on_raw_reaction_remove(payload):
    await on_reaction(payload, Context.emoji_remove)

@client.event
async def on_message(message):
    if type(message.channel) in supported_channels_tyes + [discord.channel.DMChannel]:
        if is_mention(message, client.user):
            await on_config_message(client, message)
    if type(message.channel) in supported_channels_tyes:
        context = Context.auto
        #await message.guild.chunk();
        # print(client.user)
        # print(message.mentions)
        channel = message.channel
        #print(channel.members)
        #print(len(channel.members))
        #for user in config:
            #print(int(user))
            #print(await client.fetch_user(int(user)))
            #print(channel.permissions_for(await client.fetch_user(int(user))))
        # print(message.mentions, message.role_mentions, message.mention_everyone)
        for user in channel.members:
            user_id = str(user.id)
            if await if_receive(channel, user, "no", context, message):
                await send_messages_to_mail([message],user_id,context=context) 

@client.event
async def on_raw_message_edit(payload):
    channel = await client.fetch_channel(payload.channel_id)
    if type(channel) in supported_channels_tyes:
        context = Context.edit
        message = await channel.fetch_message(payload.message_id)
        for user in channel.members:
            user_id = str(user.id)
            if await if_receive(channel, user, "no", context, message):
                await send_messages_to_mail([message],user_id,context=context) 

@client.event
async def on_raw_message_delete(payload):
    channel = await client.fetch_channel(payload.channel_id)
    if type(channel) in supported_channels_tyes:
        context = Context.delete
        for user in channel.members:
            user_id = str(user.id)
            if await if_receive(channel, user, "no", context):
                print(payload)
                await send_discord_mail(user_id, " DELETED ", channel=channel, m_id=payload.message_id, in_reply_to_id=payload.message_id, context=context)


client.run(secret.token)
