from enum import Enum

class Context(Enum):
    default = ""
    auto = "auto"
    manual = "manual"
    edit = "edit"
    delete = "delete"
    emoji_add = "emoji_add"
    emoji_remove = "emoji_remove"

class OptionItem:
    def __init__(self,name:str, help_str:str, validate):
        self.name = name
        self.help = help_str
        self.validate = validate

def option_enum_val(name:str, l:list[str]):
    return OptionItem(name," | ".join(l),lambda x:x in l)

available_options= [
        OptionItem("mail","<mail@addres>",lambda x: '@' in x),
        OptionItem("history_lines","<int>",lambda x: x.isdecimal()),
        OptionItem("receive",
            "no | <string of chars: a - all, m, M - mention, d, D - direct mention, w - watch emoji, r - reply to my message>",
            lambda x: x == "no" or all([i in "amMdDwr" for i in x]))
        ]

available_options_dict = {i.name:i for i in available_options}

Option = Enum("Option", [i.name for i in available_options ])
