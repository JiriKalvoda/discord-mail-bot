Installation and running
=======================

1) Create discord bot
	1) Setup bot on Discord pages.
	2) Write `token = '<your token>'` to `secret.py`

2) Configure mail 
	1) Configure `neomutt` for sending mails
	2) Configure bot settings:
		Write next lines to `mail_config.py`
		```
		mail_from_address = "<your.mail+address@on.your.server"
		mail_message_id_suffix = ".unique.suffix@on.your.server"
		```
3) Create empty config: Write `{}` to `config.json`

4) Create daemon or simply run `python3 main.py`
