import discord
import os
import datetime
import json
from enum import Enum

from config_options import available_options, available_options_dict, Context, Option


supported_channels_tyes=[discord.channel.TextChannel]



config_file = "config.json"

with open(config_file) as in_file:
    config = json.load(in_file)

def config_write():
    with open(config_file, 'w') as out_file:
        json.dump(config, out_file)

def get_id_list(channel, length=4):
    if length == 0:
        return []
    return list(map(str,[channel.guild.id, channel.category.id if channel.category else "none", channel.id][0:length]))

def get_config(user_id : str, channel, key:Option, default=None, context:Context=Context.default):
    # print("get_config", user_id, channel, type(key),key, default, context)
    r = default
    if user_id not in config:
        return r
    c = config[user_id]
    id_list = get_id_list(channel)
    def do_node(c):
        nonlocal r
        try:
            r = c["c"][Context.default.name][key.name]
        except:
            pass
        try:
            r = c["c"][context.name][key.name]
        except:
            pass
    do_node(c)
    for act_id in id_list:
        if act_id not in c["s"]:
            return r
        c = c["s"][act_id]
        do_node(c)
        # print(" -> ", r)
    return r

def level_id(level):
    assert(level in ["global","guild","category","channel"])
    return {"global":0, "guild":1, "category":2, "channel":3}[level]


def set_config(user_id : str, channel, level:str,  key:Option, val:str = "", recursive = False, delete=False, contexts:list[Context] = [Context.default]):
    for context in contexts:
        c = config.setdefault(user_id,{"c":{},"s":{}})
        id_list = get_id_list(channel, level_id(level))
        for act_id in id_list[0:level_id(level)]:
            c = c["s"].setdefault(act_id,{"c":{},"s":{}})
        if delete:
            c["c"].setdefault(context.name,{}).pop(key.name,None)
        else:
            c["c"].setdefault(context.name,{})[key.name]=val
        if recursive:
            def r(c,key):
                for i in c["s"].values():
                    i["c"].setdefault(context.name,{}).pop(key.name,None)
                    r(i,key)
            r(c,key)
    config_write()

async def ids_to_name(client, meet_ids):
    if len(meet_ids) == 1:
        name = client.get_guild(int(meet_ids[0])).name
    if len(meet_ids) == 2:
        name = [x for x in client.get_guild(int(meet_ids[0])).categories if x.id == int(meet_ids[1])][0].name
    try:
        if len(meet_ids) == 3:
            name = client.get_channel(int(meet_ids[2])).name
        return f"**{name}** ({meet_ids[-1]}):"
    except:
        return f"**{meet_ids[-1]}** (unknow name):"

async def print_config(client, user_id : str, channel, level:str, recursive = False):
    # print([user_id, channel, level_id, recursive])
    out = ""
    one_tab="\t\t\t"
    meet_ids = []
    def print_config_node(c, tab):
        nonlocal out
        for (context,key_val) in c.items():
            for (key,val) in key_val.items():
                out += one_tab*tab + f"-> {context} {key}: {val}\n"
    if user_id not in config:
        return "Empty. Your user don't have any configuration."
    c = config[user_id]
    out += f"**Default:**\n"
    print_config_node(c["c"], 0)
    id_list = get_id_list(channel, level_id(level))
    for act_id in id_list:
        if act_id not in c["s"]:
            return out
        meet_ids.append(act_id)
        c = c["s"][act_id]
        out += "\n"+await ids_to_name(client, meet_ids)+"\n"
        print_config_node(c["c"], 0)

    if recursive:
        async def r(c, tab, meet_ids):
            nonlocal out
            for (act_id,i) in c["s"].items():
                out += "\n"+one_tab*tab+await ids_to_name(client, meet_ids+[act_id])+"\n"
                print_config_node(i["c"],tab)
                await r(i,tab+1, meet_ids+[act_id])
        await r(c, 1, meet_ids)
    return out



async def send_help(message):
    await message.reply("""
Command unrecognized.

Usage:
<bot_mention> [goto channel id] [recursive] <level> set [<context>] [...] <option> <value>
<bot_mention> [goto channel id] [recursive] <level> delete [<context>] [...] <option>
<bot_mention> [goto channel id] [recursive] <level> print

level: global | guild | category | channel

content: """ +" | ".join([f"{i.name}" for i in Context])+ """

option:
""" + "\n".join([f"{i.name}: {i.help}" for i in available_options]) +""" 
            """)

async def send_ok(message):
    await message.reply("OK")


async def on_config_message(client, message):
    arg = list(filter(len, message.content.split(" ")[1:]))
    user = str(message.author.id)
    # print(arg)
    working_channel = message.channel
    if len(arg) == 0:
        await send_help(message)
        return
    if len(arg)>=1 and all([i in "0123456789" for i in arg[0]]):
        try:
            working_channel = await client.fetch_channel(int(arg[0]))
            arg = arg[1:]
        except:
            await message.reply("Failed to set working channel.")
            return
    recursive = len(arg) and arg[0]=="recursive"
    if recursive:
        arg = arg[1:]
    if len(arg)>=1 and arg[0] in ["global","guild","category","channel"]:
        level=arg[0]
        arg = arg[1:]
        if level != "global" and type(working_channel) not in supported_channels_tyes:
            await message.reply("Working channel type is not supported.")
            return

        def parse_contexts(arg):
            if len(arg) == 0:
                return [Context.default]
            r=[]
            for i in arg:
                try:
                    r.append(Context[i])
                except:
                    message.reply("Invalid context.")
                    return None
            return r
                

        if len(arg)>=3 and arg[0] == "set" and arg[-2] in available_options_dict:
            if available_options_dict[arg[-2]].validate(arg[-1]):
                contexts = parse_contexts(arg[1:-2])
                if contexts == None:
                    return
                set_config(user, working_channel,level, Option[arg[-2]], val=arg[-1], recursive=recursive, contexts=contexts)
                await send_ok(message)
            else:
                await message.reply(f"{arg[-2]}: Invalid value: please use format: {available_options_dict[arg[-2]].help}")
        elif len(arg)>=2 and arg[0] == "delete" and arg[-1] in available_options_dict:
            contexts = parse_contexts(arg[1:-1])
            if contexts == None:
                return
            set_config(user, working_channel, level, Option[arg[-1]], recursive=recursive, delete=True, contexts=contexts)
            await send_ok(message)
        elif len(arg) in [1] and arg[0] == "print":
            await message.reply(await print_config(client, user, working_channel, level, recursive=recursive))
        else:
            await send_help(message)
    else:
        await send_help(message)
