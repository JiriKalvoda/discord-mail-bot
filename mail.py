import discord
import os
import subprocess
from textwrap import *
from enum import Enum
import tempfile
from datetime import datetime
import random

from config import Context, Option, on_config_message, get_config
from constants_emoji import used_emoji
from mail_config import mail_from_address, mail_message_id_suffix




wraper = TextWrapper(width=70, initial_indent='', subsequent_indent='',
        expand_tabs=False, replace_whitespace=False, fix_sentence_endings=False,
        break_long_words=False, drop_whitespace=False, break_on_hyphens=True,
        tabsize=8,  max_lines=None, placeholder=' [...]')

default_from_name = "Discord"


def no_quotes(a):
    return "".join([i for i in a if i not in "\""])

async def send_mail(mail, subject, text, message_id, in_reply_to=None, context=Context.default, attach=[], from_name = default_from_name, headers=[]):
    with subprocess.Popen(
        ["neomutt"] +
        # From
        ["-e",f"set from = \"{no_quotes(from_name)}  <{mail_from_address}>\""] +
        # Do not record
        ["-e","unset record"] +
        ["-e","unmy_hdr bcc"] +
        # ID
        ["-e",f"my_hdr Message-ID: <{message_id}{mail_message_id_suffix}>"] +
        # Replay to
        ( ["-e","my_hdr In-Reply-To: <{in_reply_to}{mail_message_id_suffix}>"] if in_reply_to else []) +
        # Subject
        sum([["-e","my_hdr "+i] for i in headers],[]) +
        ["-s",subject] +
        # Attachments
        ( ["-a" ] + attach + ["--"] if len(attach)>0 else []) +
        [mail]
            , stdin=subprocess.PIPE) as p:
            p.stdin.write(bytes(text,'utf-8'))

def discord_headers(messages_id, channel, context=Context.default):
    guild = channel.guild
    category = channel.category
    return (
        ["Discord-Guild: "+str(guild.id)] +
        (["Discord-Category: "+str(category.id)] if category else []) +
        ["Discord-Channel: "+str(channel.id)] +
        ["Discord-Guild-Name: "+str(guild.name)] +
        (["Discord-Category-Name: "+str(category.name)] if category else []) +
        ["Discord-Channel-Name: "+str(channel.name)] +
        ["Discord-Context: "+context.name] +
        (["Discord-Messages: "+",".join([str(i) for i in messages_id])] if messages_id else [])+
            [])

async def msg_to_text(m, user_id, context=Context.default):
    text=""

    if(m.reference):
        try:
            mm = await m.channel.fetch_message(m.reference.message_id)
            t = (await msg_to_text(mm, user_id, context=context)).split("\n");
            history_lines = get_config(user_id, m.channel, Option.history_lines, 100, context)
            if history_lines != None and len(t) > int(history_lines):
                t = t[0:int(history_lines)//2] + ["[...]"] + t[-int(history_lines)//2:]
        except discord.errors.NotFound:
            t = ["[404: Not found]"]
        text += "\n".join([ '> '+ i for i in t])+"\n"

    text += "\n"
    text += m.author.name+":\n"

    text += "\n".join(["\n".join(wraper.wrap(x)) for x in m.content.split("\n")])

    for r in m.reactions:
        if str(r.emoji) not in used_emoji:
            text += "\n"
            text += "> " + str(r.emoji) + ":"
            async for user in r.users():
                text += " " + str(user)
    return text

async def send_messages_to_mail(messages, user_id, context=Context.default):
    m = messages[0];
    mail = get_config(user_id, m.channel, Option.mail, "", context)
    c = m.channel
    if mail == "":
        return
    text = ""
    attach = []
    tmpdir = None
    for m in reversed(messages):
        text += await msg_to_text(m, user_id, context=context)
        text += "\n"
        for a in m.attachments:
            if not tmpdir:
                tmpdir = tempfile.mkdtemp()+"/"
            #print("tmpdir:",tmpdir)
            text += "["+a.filename+"]\n"
            f = tmpdir+a.filename
            #print("f: ",f); 
            if not os.path.exists(tmpdir):
                os.mkdir(tmpdir)
            await a.save(f)
            attach.append(f)
    text += "\n"
    text += "\n"
    text += m.jump_url
    message_id = f"{m.id}.{m.channel.id}"
    in_reply_to = None
    if m.reference:
        in_reply_to = f"{m.reference.message_id}.{m.channel.id}"
    if context == Context.edit:
        message_id = str(random.randint(0,999))+"."+datetime.now().strftime("%Y-%m-%d--%H-%M-%S")+"."+f"{m.id}.{m.channel.id}"
        in_reply_to = f"{m.id}.{m.channel.id}"
    subject = "[Discord] "+m.guild.name+(" -> "+m.channel.category.name if m.channel.category else "")+" -> "+m.channel.name
    from_name = default_from_name if len(messages)>1 else m.author.name
    headers = discord_headers([i.id for i in messages], m.channel, context=context)
    await send_mail(mail, subject, text, message_id, in_reply_to=in_reply_to, context=context, attach=attach, from_name=from_name, headers=headers)
    for f in attach:
        #print("rm ", f)
        os.remove(f)
    if tmpdir:
        os.rmdir(tmpdir)

async def send_discord_mail(user_id, text, channel, m_id, in_reply_to_id, context=Context.default, from_name=default_from_name):
    mail = get_config(user_id, channel, Option.mail, "", context)
    c = channel
    if mail == "":
        return
    message_id = str(random.randint(0,999))+"."+datetime.now().strftime("%Y-%m-%d--%H-%M-%S")+"."+f"{m_id}.{channel.id}"
    in_reply_to = f"{in_reply_to_id}.{channel.id}"
    subject = "[Discord] "+channel.guild.name+(" -> "+channel.category.name if channel.category else "")+" -> "+channel.name
    headers = discord_headers([m_id], channel, context=context)
    await send_mail(mail, subject, text, message_id, in_reply_to=in_reply_to, context=context, headers=headers, from_name=from_name)
